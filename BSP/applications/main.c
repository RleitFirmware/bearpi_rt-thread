/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2019-01-11     RiceChen    first edition
 */

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

/* defined the LED pin: PC13 */
#define LED0_PIN           GET_PIN(C, 13)

/* defined the KEY pin: PB2 PB3*/

#define KEY1_PIN           GET_PIN(B, 2)
#define KEY2_PIN           GET_PIN(B, 3)

//按键中断处理函数
static void key1_irq_handler(void)
{
		int key1_value = -1;

		rt_thread_mdelay(20);
		
		key1_value = rt_pin_read(KEY1_PIN);
		
		if(key1_value == 0)
		{
				rt_kprintf(">>> key1_irq_handler >>> key1 down >>>\n");
		}
}


static void key2_irq_handler(void)
{
		int key2_value = -1;

		rt_thread_mdelay(20);
	
		key2_value = rt_pin_read(KEY2_PIN);
		
		if(key2_value == 0)
		{
				rt_kprintf(">>> key2_irq_handler >>> key2 down >>>\n");
		}
}


int main(void)
{
  int count = 1;

  rt_kprintf("---Welcome use BearPi---\n");
  /* set LED pin mode to output */
  rt_pin_mode(LED0_PIN, PIN_MODE_OUTPUT);

	//初始化按键中断
	rt_pin_attach_irq(KEY1_PIN,PIN_IRQ_MODE_FALLING,(void*)&key1_irq_handler,NULL);
	rt_pin_irq_enable(KEY1_PIN,PIN_IRQ_ENABLE);
	
	rt_pin_attach_irq(KEY2_PIN,PIN_IRQ_MODE_FALLING,(void*)&key2_irq_handler,NULL);
	rt_pin_irq_enable(KEY2_PIN,PIN_IRQ_ENABLE);
	
  while (count++)
  {
    rt_pin_write(LED0_PIN, PIN_HIGH);
	
    rt_thread_mdelay(1000);
		
    rt_pin_write(LED0_PIN, PIN_LOW);
	
    rt_thread_mdelay(1000);						
  }

  return RT_EOK;
}
