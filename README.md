# Bear-Pi 开发板说明

## 开发板介绍

对于 Bear-Pi，内核是 Cortex-M4，这款芯片是低功耗系列，板载 ST-LINK/V2-1 调试器/编程器，迷你尺寸，mirco USB 接口，可数的外设。

## 硬件支持

| 功能 | 支持情况 |
|------|----------|
| GPIO | √ |
| UART | √ |
| SPI | 支持SPI2 |
| LCD | √ |
| WIFI | Support ESP8266 WiFi Module |

## 联系人信息

维护人:

-  [RiceChen](https://github.com/RiceChen), 邮箱：<980307037@qq.com>
-  [EmbedIoT Studio](https://github.com/embediot), Email: embediot@163.com